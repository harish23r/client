import React, { Component } from 'react';
import Transition from './transition';

class Layout extends Component {
    render() {
        return (
            <div>
                <Transition />
            </div>
        );
    }
}

export default Layout; 
