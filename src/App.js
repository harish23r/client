import React, { Component } from 'react';
import logo from './logo.svg';
import Layout from './Components/layout.js'
import './App.css';

class App extends Component {
  render() {
    return (
      <Layout />
    );
  }
}

export default App;
